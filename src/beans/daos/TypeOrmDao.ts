import { BaseEntity, DataSource, EntityTarget, Repository, SelectQueryBuilder } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { IDatabaseService } from '../../services/DatabaseService/IDatabaseService';
import { Dao } from './Dao';

type IEntity<T> = QueryDeepPartialEntity<T>

export class TypeOrmDao<T extends BaseEntity> extends Dao<DataSource> {

  protected schema: EntityTarget<T>;
  protected model: Repository<T>;
  protected createQueryBuilder: () => SelectQueryBuilder<T>;

  constructor(databaseService: IDatabaseService<DataSource>, schema: EntityTarget<T>) {
    super(databaseService);
    this.schema = schema;
    this.model = this.databaseConnexion.getRepository(this.schema);
    this.createQueryBuilder = this.model.createQueryBuilder.bind(this.model);
  }

  createOne(body: IEntity<T>) {
    return this.databaseConnexion.createQueryBuilder()
      .insert()
      .into(this.schema)
      .values(body)
      .execute();
  }

  createMany(body: IEntity<T>[]) {
    return this.databaseConnexion.createQueryBuilder()
      .insert()
      .into(this.schema)
      .values(body)
      .execute();
  }

  findOneById(id: unknown) {
    return this.databaseConnexion.createQueryBuilder()
      .select('entity')
      .from(this.schema, 'entity')
      .where('entity.id = :id', { id })
      .getOne();
  }

  findAll(id: unknown) {
    return this.databaseConnexion.createQueryBuilder()
      .select('entity')
      .from(this.schema, 'entity')
      .getMany();
  }

  updateOneById(id: unknown, body: Partial<IEntity<T>>) {
    return this.databaseConnexion.createQueryBuilder()
      .update(this.schema)
      .set(body)
      .where('id = :id', { id })
      .execute();
  }

  deleteOneById(id: unknown) {
    return this.databaseConnexion.createQueryBuilder()
      .delete()
      .from(this.schema)
      .where('id = :id', { id })
      .execute();
  }

}
