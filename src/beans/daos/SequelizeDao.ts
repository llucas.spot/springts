import {
  CreationAttributes,
  InitOptions,
  Model,
  ModelStatic,
  Sequelize,
  Transaction,
} from 'sequelize';
import {
  Attributes,
  BulkCreateOptions,
  CreateOptions,
  FindOptions,
  FindOrCreateOptions,
  Identifier,
  InstanceDestroyOptions,
  InstanceUpdateOptions,
  NonNullFindOptions,
} from 'sequelize/types/model';
import { Col, Fn, Literal } from 'sequelize/types/utils';
import { IDatabaseService } from '../../services/DatabaseService/IDatabaseService';
import { Dao } from './Dao';

export type DaoOptions<T> = { transaction: T }

export type SequelizeInitOptions = Omit<InitOptions, 'sequelize' | 'modelName' | 'tableName'>

export abstract class SequelizeModelI {
  abstract init(sequelize: Sequelize, options?: SequelizeInitOptions): void

  abstract setAssociations(): void

  abstract initSafe(cb: () => void): void

  abstract setAssociationsSafe(cb: () => void): void
}

export class SequelizeModel {
  private isInitialized: boolean = false;
  private areAssociationsSetted: boolean = false;

  initSafe(cb: () => void) {
    if (this.isInitialized) {
      return;
    }
    cb();
    this.isInitialized = true;
  }

  setAssociationsSafe(cb: () => void) {
    if (this.areAssociationsSetted) {
      return;
    }
    cb();
    this.areAssociationsSetted = true;
  }

}

type UpdateBody<TModelAttributes> = { [key in keyof TModelAttributes]?: TModelAttributes[key] | Fn | Col | Literal; }
type NonFunctionPropertyNames<T> = { [K in keyof T]: T[K] extends Function ? never : T[K] };

// @ts-ignore
export class SequelizeDao<TModelAttributes extends {} = any, TCreationAttributes extends {} = TModelAttributes, T extends Model<TModelAttributes, TCreationAttributes>> extends Dao<Sequelize> {

  protected model: ModelStatic<T>;

  protected arrayToJSON(instances: T[]) {
    return instances.map(this.instanceToJSON);
  }

  protected instanceToJSON(instance: T) {
    return instance.toJSON<TModelAttributes>();
  }

  protected instanceOrNullToJSON(instance: T | null) {
    if (!instance) {
      return null;
    }
    return instance.toJSON<TModelAttributes>();
  }

  protected async getTransaction(transaction: Transaction | null | undefined): Promise<{ transaction: Transaction, isTransactionNew: boolean }> {
    if (transaction) {
      return { transaction, isTransactionNew: false };
    }
    const newtransaction = await this.databaseConnexion.transaction();
    return {
      transaction: newtransaction, isTransactionNew: true,
    };
  }

  constructor(databaseService: IDatabaseService<Sequelize>, model: ModelStatic<T>) {
    super(databaseService);
    this.model = model;
  }

  createOne(body: CreationAttributes<T>): Promise<TModelAttributes>
  createOne(body: CreationAttributes<T>, options: CreateOptions<Attributes<T>>): Promise<T>
  createOne(body: CreationAttributes<T>, options?: CreateOptions<Attributes<T>>): Promise<T | TModelAttributes> {
    return this.withSafeTransaction(options?.transaction, async ({
      transaction,
      isTransactionNew,
    }) => {
      const promise = this.model.create(body, { ...options, transaction });
      if (isTransactionNew) {
        return promise.then(this.instanceToJSON);
      }
      return promise;
    });
  }

  findOrCreate(
    createOptions: Omit<FindOrCreateOptions<Attributes<T>, CreationAttributes<T>>, 'transaction'>,
  ): Promise<[TModelAttributes, boolean]>
  findOrCreate(
    createOptions: Omit<FindOrCreateOptions<Attributes<T>, CreationAttributes<T>>, 'transaction'>,
    options: DaoOptions<Transaction>,
  ): Promise<[T, boolean]>
  findOrCreate(
    createOptions: FindOrCreateOptions<Attributes<T>, CreationAttributes<T>>,
    options?: DaoOptions<Transaction>,
  ): Promise<[T | TModelAttributes, boolean]> {
    return this.withSafeTransaction(options?.transaction, async ({
      transaction,
      isTransactionNew,
    }) => {
      const promise = this.model.findOrCreate({ ...createOptions, transaction });
      if (isTransactionNew) {
        return promise.then(([instance, isCreated]) => [this.instanceToJSON(instance), isCreated]);
      }
      return promise;
    });
  }

  createMany(body: CreationAttributes<T>[]): Promise<TModelAttributes[]>
  createMany(body: CreationAttributes<T>[], options: BulkCreateOptions<Attributes<T>>): Promise<T[]>
  createMany(body: CreationAttributes<T>[], options?: BulkCreateOptions<Attributes<T>>):  Promise<TModelAttributes[] | T[]> {
    return this.withSafeTransaction(options?.transaction, async ({
      transaction,
      isTransactionNew,
    }) => {
      const promise = this.model.bulkCreate(body, options);
      if (isTransactionNew) {
        return promise.then((result) => this.arrayToJSON(result));
      }
      return promise;
    });
  }

  findOne(): Promise<TModelAttributes | null>
  findOne(options: FindOptions<Attributes<T>>): Promise<T | null>
  findOne(options?: FindOptions<Attributes<T>>): Promise<T | TModelAttributes | null> {
    return this.withSafeTransaction(options?.transaction, async ({
      transaction,
      isTransactionNew,
    }) => {
      const promise = this.model.findOne(options);
      if (isTransactionNew) {
        return promise.then(this.instanceOrNullToJSON);
      }
      return promise;
    });
  }

  findOneByPk(pk: Identifier): Promise<TModelAttributes | null>
  findOneByPk(pk: Identifier, options: Omit<NonNullFindOptions<Attributes<T>>, 'where'>): Promise<T | null>
  findOneByPk(pk: Identifier, options?: Omit<NonNullFindOptions<Attributes<T>>, 'where'>): Promise<TModelAttributes | T | null> {
    return this.withSafeTransaction(options?.transaction, async ({
      transaction,
      isTransactionNew,
    }) => {
      const promise = this.model.findByPk(pk, options);
      if (isTransactionNew) {
        return promise.then(this.instanceOrNullToJSON);
      }
      return promise;
    });
  }

  findAll(): Promise<TModelAttributes[]>
  findAll(options: FindOptions<Attributes<T>>): Promise<T[]>
  findAll(options?: FindOptions<Attributes<T>>): Promise<TModelAttributes[] | T[]> {
    return this.withSafeTransaction(options?.transaction, async ({
      transaction,
      isTransactionNew,
    }) => {
      const promise = this.model.findAll(options);
      if (isTransactionNew) {
        return promise.then((result) => this.arrayToJSON(result));
      }
      return promise;
    });
  }

  findAndCountAll(): Promise<{ rows: TModelAttributes[], count: number }>
  findAndCountAll(options: FindOptions<Attributes<T>>): Promise<{ rows: T[], count: number }>
  findAndCountAll(options?: FindOptions<Attributes<T>>): Promise<{ rows: TModelAttributes[] | T[], count: number }> {
    return this.withSafeTransaction(options?.transaction, async ({
      transaction,
      isTransactionNew,
    }) => {
      const promise = this.model.findAndCountAll(options);
      if (isTransactionNew) {
        return promise.then((result) => {
          return { rows: this.arrayToJSON(result.rows), count: result.count };
        });
      }
      return promise;
    });
  }

  async updateOneByPk(pk: Identifier, body: UpdateBody<TModelAttributes>, options?: InstanceUpdateOptions<TModelAttributes>): Promise<T | null> {
    const instance = await this.model.findByPk(pk);
    await instance?.update(body, options);
    return instance!;
  }

  async deleteOneByPk(pk: Identifier, options?: InstanceDestroyOptions): Promise<T | null> {
    const instance = await this.model.findByPk(pk);
    await instance?.destroy(options);
    return instance!;
  }

  protected async withSafeTransaction<T>(_transaction: Transaction | null | undefined, cb: (transactionParams: { transaction: Transaction, isTransactionNew: boolean }) => Promise<T>): Promise<T> {
    const { transaction, isTransactionNew } = await this.getTransaction(_transaction);
    try {
      const promise = await cb({ transaction, isTransactionNew });
      if (isTransactionNew) {
        await transaction.commit();
      }
      return promise;
    }
    catch (error) {
      if (isTransactionNew) {
        await transaction.rollback();
      }
      throw error;
    }
  }

}
