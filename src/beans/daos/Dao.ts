import { IDatabaseService } from '../../services/DatabaseService/IDatabaseService';
import { Base } from '../Base';

export class Dao<T> extends Base {
  protected databaseConnexion = this.databaseService.getDatabaseConnexion();

  constructor(protected databaseService: IDatabaseService<T>) {
    super();
  }
}
