export enum ColumnType {
  STRING = 'varchar',
  ID = 'bigint',
  NUMBER = 'int',
  TIMESTAMP = 'timestamp',
}
