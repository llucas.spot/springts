import { ColumnOptions } from 'typeorm/decorator/options/ColumnOptions';
import { ColumnType } from '../ColumnType';
import { ColumnTypeWidth } from '../ColumnTypeWidth';

export const typeormColumnStringOptions: ColumnOptions = {
  type: ColumnType.STRING,
  width: ColumnTypeWidth.STRING,
};

export const typeormColumnStringRequiredOptions: ColumnOptions = {
  ...typeormColumnStringOptions,
  nullable: false,
};
