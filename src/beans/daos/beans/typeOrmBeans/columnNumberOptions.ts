import { ColumnOptions } from 'typeorm/decorator/options/ColumnOptions';
import { ColumnType } from '../ColumnType';
import { ColumnTypeWidth } from '../ColumnTypeWidth';

export const typeormColumnNumberRequiredOptions: ColumnOptions = {
  type: ColumnType.NUMBER,
  width: ColumnTypeWidth.NUMBER,
  nullable: false,
};
