import {
  PrimaryGeneratedColumnNumericOptions,
} from 'typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions';
import { ColumnType } from '../ColumnType';
import { ColumnTypeWidth } from '../ColumnTypeWidth';

export const typeormPrimaryGeneratedColumnIdOptions: PrimaryGeneratedColumnNumericOptions = {
  type: ColumnType.ID,
  width: ColumnTypeWidth.ID,
  nullable: false,
} as PrimaryGeneratedColumnNumericOptions;
