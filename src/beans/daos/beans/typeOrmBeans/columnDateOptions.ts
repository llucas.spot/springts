import { ColumnOptions } from 'typeorm/decorator/options/ColumnOptions';
import { ColumnType } from '../ColumnType';
import { ColumnTypeDefault } from '../ColumnTypeDefault';

export const typeormColumnDateCreatedAtOptions: ColumnOptions = {
  type: ColumnType.TIMESTAMP,
  default: () => ColumnTypeDefault.CURRENT_TIMESTAMP,
};

export const typeormColumnDateUpdatedAtOptions: ColumnOptions = {
  type: ColumnType.TIMESTAMP,
  default: () => ColumnTypeDefault.CURRENT_TIMESTAMP,
  onUpdate: ColumnTypeDefault.CURRENT_TIMESTAMP,
};

export const typeormColumnDateDeletedAtOptions: ColumnOptions = {
  type: ColumnType.TIMESTAMP,
};
