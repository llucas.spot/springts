export enum ColumnTypeWidth {
  STRING = 255,
  ID = 80,
  NUMBER = 32,
}
