import { DataTypes } from 'sequelize';
import { ModelAttributeColumnOptions } from 'sequelize/types/model';
import { ColumnTypeWidth } from '../ColumnTypeWidth';

export const sequelizeColumnStringOptions = (): ModelAttributeColumnOptions => {
  return {
    type: DataTypes.STRING({
      length: ColumnTypeWidth.STRING,
    }),
  };
};

export const sequelizeColumnStringRequiredOptions = (): ModelAttributeColumnOptions => {
  return {
    ...sequelizeColumnStringOptions(),
    allowNull: false,
  };
};
