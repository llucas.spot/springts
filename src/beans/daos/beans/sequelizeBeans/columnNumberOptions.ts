import { DataTypes } from 'sequelize';
import { ModelAttributeColumnOptions } from 'sequelize/types/model';
import { ColumnTypeWidth } from '../ColumnTypeWidth';

export const sequelizeColumnNumberOptions = (): ModelAttributeColumnOptions => {
  return {
    type: DataTypes.INTEGER({
      length: ColumnTypeWidth.NUMBER,
    }),
  };
};

export const sequelizeColumnNumberRequiredOptions = (): ModelAttributeColumnOptions => {
  return {
    ...sequelizeColumnNumberOptions(),
    allowNull: false,
  };
};
