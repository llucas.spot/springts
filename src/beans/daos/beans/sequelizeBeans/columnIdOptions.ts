import { DataTypes } from 'sequelize';
import { ModelAttributeColumnOptions } from 'sequelize/types/model';
import { ColumnTypeWidth } from '../ColumnTypeWidth';

export const sequelizePrimaryColumnIdOptions = (): ModelAttributeColumnOptions => {
  return {
    type: DataTypes.BIGINT({
      length: ColumnTypeWidth.ID,
    }).UNSIGNED,
  };
};

export const sequelizePrimaryColumnIdRequiredOptions = (): ModelAttributeColumnOptions => {
  return {
    ...sequelizePrimaryColumnIdOptions(),
    allowNull: false,
  };
};

export const sequelizePrimaryColumnIdRequiredJoinTableOptions = (): ModelAttributeColumnOptions => {
  return {
    ...sequelizePrimaryColumnIdRequiredOptions(),
    primaryKey: true,
  };
};

export const sequelizePrimaryGeneratedColumnIdOptions = (): ModelAttributeColumnOptions => {
  return {
    ...sequelizePrimaryColumnIdOptions(),
    autoIncrement: true,
    primaryKey: true,
  };
};
