import { Router } from 'express';
import { ExpressFunction } from '../decorators/base/beans';
import { MetadataService } from '../services/MetadataService/MetadataService';
import { Base } from './Base';

function normalize (strArray: string[]) {
  const resultArray = [];
  if (strArray.length === 0) { return ''; }

  if (typeof strArray[0] !== 'string') {
    throw new TypeError('Url must be a string. Received ' + strArray[0]);
  }

  // If the first part is a plain protocol, we combine it with the next part.
  if (strArray[0].match(/^[^/:]+:\/*$/) && strArray.length > 1) {
    strArray[0] = strArray.shift() + strArray[0];
  }

  // There must be two or three slashes in the file protocol, two slashes in anything else.
  if (strArray[0].match(/^file:\/\/\//)) {
    strArray[0] = strArray[0].replace(/^([^/:]+):\/*/, '$1:///');
  } else {
    strArray[0] = strArray[0].replace(/^([^/:]+):\/*/, '$1://');
  }

  for (let i = 0; i < strArray.length; i++) {
    let component = strArray[i];

    if (typeof component !== 'string') {
      throw new TypeError('Url must be a string. Received ' + component);
    }

    if (component === '') { continue; }

    if (i > 0) {
      // Removing the starting slashes for each component but the first.
      component = component.replace(/^[\/]+/, '');
    }
    if (i < strArray.length - 1) {
      // Removing the ending slashes for each component but the last.
      component = component.replace(/[\/]+$/, '');
    } else {
      // For the last component we will combine multiple slashes to a single one.
      component = component.replace(/[\/]+$/, '/');
    }

    resultArray.push(component);

  }

  let str = resultArray.join('/');
  // Each input component is now separated by a single slash except the possible first plain protocol part.

  // remove trailing slash before parameters or hash
  str = str.replace(/\/(\?|&|#[^!])/g, '$1');

  // replace ? in parameters with &
  const parts = str.split('?');
  str = parts.shift() + (parts.length > 0 ? '?': '') + parts.join('&');

  return str;
}

export default function urlJoin(...args: string[]) {
  const parts = Array.from(Array.isArray(args[0]) ? args[0] : args);
  return normalize(parts);
}

export class Ws extends Base {
  private _router?: Router;

  constructor() {
    super();
    MetadataService.addWebserviceInstance(this);
  }

  private initRouter() {
    this.logger.info('Initialise router');
    const wsClass = this.constructor;
    const wsInformations = MetadataService.getWsInformations(wsClass);
    const wsMiddlewares = MetadataService.getWsMiddlewares(wsClass) as ExpressFunction[];
    const wsRoutes = MetadataService.getWsRoutes(wsClass);
    const wsRoutesMiddlewares = MetadataService.getWsRoutesMiddlewares(wsClass);

    this._router = Router();
    if (wsMiddlewares.length) this._router!.use(urlJoin('/', wsInformations.pathName), [
      ...wsMiddlewares.map(middleware => middleware.bind(this)),
    ]);
    const routes = Object.values(wsRoutes);
    routes.forEach((route) => {
      const routeMiddlewares = wsRoutesMiddlewares[route.name] as ExpressFunction[] || [];
      const urlPath = urlJoin('/', wsInformations.pathName, route.pathName)
      if (route.method) this._router![route.method.expressHttpMethod](urlPath, [
        // add WsMiddlewares juste to the route
        ...routeMiddlewares.map(middleware => middleware.bind(this)),
        route.method.requestHandler.bind(this),
      ]);
    });
  }

  get router() {
    if (!this._router) this.initRouter();
    return this._router as Router;
  }

}
