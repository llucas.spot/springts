import { Strategy as PassportStrategy } from 'passport';

export type StrategyI = {
  getStrategy: (...args: any[]) => PassportStrategy;
}
