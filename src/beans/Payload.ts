type JSONValue =
  | string
  | number
  | boolean
  | JSONObject
  | JSONArray;

type JSONObject = {
  [x: string]: JSONValue;
}

type JSONArray = JSONValue[]

export type Payload = JSONObject
