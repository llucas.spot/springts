import Joi from 'joi';
import { Base } from './Base';
import { Payload } from './Payload';

export class Strategy extends Base {

  validatePayloadWithJoi(payload: Payload, payloadJoiSchema: Joi.ObjectSchema): Joi.ValidationResult<{
    sub: number,
    permissions: string[],
    iat: number,
  }> {
    return payloadJoiSchema.validate(payload, {
      abortEarly: false,
    });
  }

}
