import { createLogger, format, transports } from 'winston';
import { Logger } from './Logger';

export class Base {
  protected logger = this.createLogger(this);

  private createLogger(Class: any): Logger {
    return createLogger({
      transports: [new transports.Console()],
      format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.printf(({ timestamp, level, message }) => {
          return `[${timestamp}] ${level}: ${Class.constructor.name} : ${message}`;
        }),
      ),
    });
  }
}
