export abstract class IDatabaseService<T> {
  abstract getDatabaseConnexion(): T

  abstract checkConnectionToDatabase(): Promise<any>
}
