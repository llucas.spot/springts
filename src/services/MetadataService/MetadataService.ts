import 'reflect-metadata'
import { Ws } from '../../beans/Ws';
import {
  ExpressErrorMiddleware,
  ExpressErrorMiddlewares,
  ExpressMiddleware,
  ExpressMiddlewares,
  MetadataKeys,
  RouteDefinition,
  Target,
  WsInformations,
  WsMiddlewares,
  WsRoutes,
  WsRoutesMiddlewares,
} from '../../decorators/base/beans';

const globalObject = {};

export class MetadataService {

  // getter / setter de base

  static addWebserviceInstance(ws: Ws): void {
    const wss: Ws[] = this.getWebserviceInstances();
    Reflect.defineMetadata(
      MetadataKeys.WS_INSTANCES,
      [
        ...wss,
        ws,
      ],
      globalObject,
    );
  }

  static getWebserviceInstances(): Ws[] {
    if (!Reflect.hasMetadata(MetadataKeys.WS_INSTANCES, globalObject)) Reflect.defineMetadata(
      MetadataKeys.WS_INSTANCES,
      [],
      globalObject,
    );
    return Reflect.getMetadata(MetadataKeys.WS_INSTANCES, globalObject);
  }

  static getWsInformations(target: Target): WsInformations {
    if (!Reflect.hasMetadata(MetadataKeys.WS_INFORMATIONS, target)) this.setWsInformations({
      pathName: '',
    }, target);
    return Reflect.getMetadata(MetadataKeys.WS_INFORMATIONS, target);
  }

  private static setWsInformations(value: WsInformations, target: Target): void {
    return Reflect.defineMetadata(MetadataKeys.WS_INFORMATIONS, value, target);
  }

  static getWsRoutesMiddlewares(target: Target): WsRoutesMiddlewares {
    if (!Reflect.hasMetadata(MetadataKeys.WS_ROUTES_MIDDLEWARES, target)) this.setWsRoutesMiddlewares({}, target);
    return Reflect.getMetadata(MetadataKeys.WS_ROUTES_MIDDLEWARES, target);
  }

  private static setWsRoutesMiddlewares(value: WsRoutesMiddlewares, target: Target): void {
    return Reflect.defineMetadata(MetadataKeys.WS_ROUTES_MIDDLEWARES, value, target);
  }

  static getWsMiddlewares(target: Target): WsMiddlewares {
    if (!Reflect.hasMetadata(MetadataKeys.WS_MIDDLEWARES, target)) this.setWsMiddlewares([], target);
    return Reflect.getMetadata(MetadataKeys.WS_MIDDLEWARES, target);
  }

  private static setWsMiddlewares(value: WsMiddlewares, target: Target): void {
    return Reflect.defineMetadata(MetadataKeys.WS_MIDDLEWARES, value, target);
  }

  static getWsRoutes(target: Target): WsRoutes {
    if (!Reflect.hasMetadata(MetadataKeys.WS_ROUTES, target)) this.setWsRoutes({}, target);
    return Reflect.getMetadata(MetadataKeys.WS_ROUTES, target);
  }

  private static setWsRoutes(value: WsRoutes, target: Target): void {
    return Reflect.defineMetadata(MetadataKeys.WS_ROUTES, value, target);
  }

  // autres getter

  static getWsRouteMiddlewares(routeName: string, target: Target): ExpressMiddlewares {
    const wsRoutesMiddlewares = this.getWsRoutesMiddlewares(target);
    return wsRoutesMiddlewares[routeName] || [];
  }

  static getWsRoute(routeName: string, target: Target): Partial<RouteDefinition> {
    const wsRoutes = this.getWsRoutes(target);
    return wsRoutes[routeName] || { pathName: '', name: routeName };
  }

  // add

  static addWsRouteInformations(routeName: string, routeInformationsToAdd: Partial<RouteDefinition>, target: Target) {
    const wsRoutes = this.getWsRoutes(target);
    wsRoutes[routeName] = {
      ...wsRoutes[routeName] || { pathName: '', name: routeName },
      ...routeInformationsToAdd,
    };
    this.setWsRoutes(wsRoutes, target);
  }

  static addWsInformations(wsInformationsToAdd: Partial<WsInformations>, target: Target) {
    const wsInformations = {
      ...this.getWsInformations(target),
      ...wsInformationsToAdd,
    };
    this.setWsInformations(wsInformations, target);
  }

  static addWsMiddleware(middleware: ExpressMiddleware | ExpressMiddlewares | ExpressErrorMiddleware | ExpressErrorMiddlewares, target: Target) {
    let wsMiddlewares = this.getWsMiddlewares(target);
    const middlewares = Array.isArray(middleware) ? middleware : [middleware];
    // @ts-ignore
    wsMiddlewares = [
      ...middlewares,
      ...wsMiddlewares,
    ];
    this.setWsMiddlewares(wsMiddlewares, target);
  }

  static addWsRouteMiddleware(middleware: ExpressMiddleware | ExpressMiddlewares | ExpressErrorMiddleware | ExpressErrorMiddlewares, routeName: string, target: Target) {
    const wsRoutesMiddlewares = this.getWsRoutesMiddlewares(target);
    if (!wsRoutesMiddlewares[routeName]) wsRoutesMiddlewares[routeName] = [];
    const middlewares = Array.isArray(middleware) ? middleware : [middleware];
    // @ts-ignore
    wsRoutesMiddlewares[routeName] = [
      ...middlewares,
      ...wsRoutesMiddlewares[routeName],
    ];
    this.setWsRoutesMiddlewares(wsRoutesMiddlewares, target);
  }

}
