import { Application, Router } from 'express';
import { Service } from '../../beans/Service';
import { Ws } from '../../beans/Ws';
import { ExpressErrorMiddlewares, ExpressNormalMiddlewares } from '../../decorators/base/beans';
import { MetadataService } from '../MetadataService/MetadataService';
import { IExpressService } from './IExpressService';

export class ExpressService extends Service implements IExpressService {
  protected application: Application;

  constructor(application: Application) {
    super();
    this.application = application;
  }

  public getApplication(): Application {
    return this.application;
  }

  protected addCommonMiddlewares(apiPath: string, middlewares: ExpressNormalMiddlewares) {
    this.logger.info('Add commons middlewares');
    this.application.use(apiPath, ...middlewares);
  }

  protected addCommonErrorMiddlewares(apiPath: string, middlewares: ExpressErrorMiddlewares) {
    this.logger.info('Add errors middlewares');
    this.application.use(apiPath, ...middlewares);
  }

  protected addRouterToApplication(apiPath: string, router: Router) {
    this.application.use(apiPath, router);
  }

  private registerWssToApplication(apiPath: string, wss: Ws[]) {
    this.logger.info('Register routers');
    const sortCallBack = (ws1: Ws, ws2: Ws) => {
      const pathName1 = MetadataService.getWsInformations(ws1.constructor).pathName;
      const pathName2 = MetadataService.getWsInformations(ws2.constructor).pathName;
      return pathName2.split('/').length - pathName1.split('/').length;
    };
    const wssWithPathParams: Ws[] = [];
    const wssWithoutPathParams: Ws[] = [];
    wss.forEach(ws => {
      const pathName = MetadataService.getWsInformations(ws.constructor).pathName;
      if (pathName.includes(':')) {
        wssWithPathParams.push(ws);
        return;
      }
      wssWithoutPathParams.push(ws);
    });
    [
      ...wssWithoutPathParams.sort(sortCallBack),
      ...wssWithPathParams.sort(sortCallBack),
    ]
      .forEach(ws => this.addRouterToApplication(apiPath, ws.router));
  }

  protected registerWebservices(apiPath: string) {
    this.registerWssToApplication(apiPath, MetadataService.getWebserviceInstances());
  }

}
