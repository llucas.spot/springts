import { Application } from 'express';

export abstract class IExpressService {
  abstract getApplication(): Application
}
