import { AuthenticateOptions } from 'passport';
import { ExpressNormalMiddleware } from '../../decorators/base/beans';

export abstract class IPassportService {

  abstract initialize(options?: { userProperty: string } | undefined): ExpressNormalMiddleware

  abstract authenticate(strategyName: string, options: AuthenticateOptions): ExpressNormalMiddleware
}
