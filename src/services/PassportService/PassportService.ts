import { AuthenticateOptions, PassportStatic, Strategy } from 'passport';
import { Service } from '../../beans';
import { ExpressNormalMiddleware } from '../../decorators';
import { IPassportService } from './IPassportService';

export class PassportService extends Service implements IPassportService {
  protected passport;

  constructor(passport: PassportStatic) {
    super();
    this.passport = passport;
  }

  protected addStrategy(strategyName: string, strategy: Strategy): void {
    this.passport.use(strategyName, strategy);
  }

  public initialize(options?: { userProperty: string } | undefined): ExpressNormalMiddleware {
    return this.passport.initialize(options);
  }

  public authenticate(strategyName: string, options: AuthenticateOptions): ExpressNormalMiddleware {
    return this.passport.authenticate(strategyName, options);
  }

}
