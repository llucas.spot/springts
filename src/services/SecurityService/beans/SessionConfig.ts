export type SessionConfig = {
    expiresIn: string;
}
