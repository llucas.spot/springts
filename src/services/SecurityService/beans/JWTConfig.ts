export type JWTConfig = {
    secret: string;
    issuer?: string;
    audience?: string;
}
