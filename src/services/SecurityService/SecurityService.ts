import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { Payload } from '../../beans/Payload';
import { Service } from '../../beans/Service';
import { HashingConfig } from './beans/HashingConfig';
import { JWTConfig } from './beans/JWTConfig';
import { SessionConfig } from './beans/SessionConfig';

abstract class ConfigurationServiceI {
  abstract getHashingConfig: () => HashingConfig;
  abstract getJWTConfig: () => JWTConfig;
  abstract getSessionConfig: () => SessionConfig;
}

export class SecurityService extends Service {

  private NUMBER_OF_ROUND_SALT = this.configurationService.getHashingConfig().numberRoundsSalt;
  private JWT_SECRET_KEY = this.configurationService.getJWTConfig().secret;
  private SESSION_DURATION = this.configurationService.getSessionConfig().expiresIn;

  constructor(protected configurationService: ConfigurationServiceI) {
    super();
  }

  bcryptHash(stringToHash: string, numberOfRoundSalt: number = this.NUMBER_OF_ROUND_SALT) {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(numberOfRoundSalt)
        .then(salt => {
          return resolve(bcrypt.hash(stringToHash, salt));
        })
        .catch(err => {
          return reject(err);
        });
    });
  }

  bcryptHashSync(stringToHash: string, numberOfRoundSalt: number = this.NUMBER_OF_ROUND_SALT) {
    const salt = bcrypt.genSaltSync(numberOfRoundSalt);
    return bcrypt.hashSync(stringToHash, salt);
  }

  bcryptCompareSync(stringToCompare: string, hashedString: string, options: { comparePlain: boolean } = { comparePlain: false }) {
    return options.comparePlain ? stringToCompare === hashedString : bcrypt.compareSync(stringToCompare, hashedString);
  }

  bcryptCompare(stringToCompare: string, hashedString: string) {
    return bcrypt.compare(stringToCompare, hashedString);
  }

  jwtGenTokenSync(payload: Payload, options = { expiresIn: this.SESSION_DURATION }, secretKey: string = this.JWT_SECRET_KEY): string {
    const token = jwt.sign(
      payload,
      secretKey,
      options,
    );
    return token;
  }

  jwtVerifyToken(token: string, secretKey: string = this.JWT_SECRET_KEY) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secretKey, (err, payload) => {
        if (err) return reject(err);
        else return resolve(payload);
      });
    });
  }

  jwtDecodeToken(token: string) {
    const decodedToken = jwt.decode(token, { complete: true });
    return decodedToken;
  }

}
