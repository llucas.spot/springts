import { Response as ExpressResponse } from 'express';

export type SpringResponse<T = undefined> = ExpressResponse<T>
