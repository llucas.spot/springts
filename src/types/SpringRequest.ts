import { Request as ExpressRequest } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';

export type SpringRequest<T = null, M = {}> = ExpressRequest<ParamsDictionary, any, T, M>
