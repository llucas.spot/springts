import { SpringRequest } from './SpringRequest';

export type SpringAuthInfo = {
  permissions: string[]
}

export type SpringAuthenticatedRequest<R, T = null, M = {}> =
  Omit<SpringRequest<T, M>, 'user' | 'authInfo'>
  & {
  user: R;
  authInfo: SpringAuthInfo
}
