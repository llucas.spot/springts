import { createLogger, format, transports } from 'winston';
import { Decorator } from '../beans';

export function addLogger(): Decorator {
  return (target: any) => {
    target.prototype.logger = createLogger({
      transports: [new transports.Console()],
      format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.printf(({ timestamp, level, message }) => {
          return `[${timestamp}] ${target.name} ${level}: ${message}`;
        }),
      ),
    });
  };
}
