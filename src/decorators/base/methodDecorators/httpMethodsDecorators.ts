import { HttpMethod } from '../beans';
import { methodDecoratorFactory } from './MethodDecoratorFactory';

export const GET = methodDecoratorFactory(HttpMethod.GET);
export const POST = methodDecoratorFactory(HttpMethod.POST);
export const PATCH = methodDecoratorFactory(HttpMethod.PATCH);
export const PUT = methodDecoratorFactory(HttpMethod.PUT);
export const DELETE = methodDecoratorFactory(HttpMethod.DELETE);
export const OPTIONS = methodDecoratorFactory(HttpMethod.OPTIONS);
