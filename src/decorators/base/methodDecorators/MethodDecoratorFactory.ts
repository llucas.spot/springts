import { MetadataService } from '../../../services/MetadataService/MetadataService';
import { Decorator, ExpressHttpMethod, HttpMethod, RouteDefinition } from '../beans';

export function methodDecoratorFactory(method: HttpMethod): Decorator {
  return (target, propertyKey, descriptor) => {
    const routeName = propertyKey!;
    const routeInformationsToAdd: Partial<RouteDefinition> = {
      method: {
        expressHttpMethod: method.toLowerCase() as ExpressHttpMethod,
        requestHandlerName: routeName,
        requestHandler: descriptor!.value,
      },
    };
    MetadataService.addWsRouteInformations(routeName, routeInformationsToAdd, target.constructor);
  };
}
