import { MetadataService } from '../../services/MetadataService/MetadataService';
import { Decorator } from './beans';

export function PathName(pathName: string = ''): Decorator {
  return (target, propertyKey, descriptor) => {
    if (propertyKey) MetadataService.addWsRouteInformations(propertyKey, { pathName }, target.constructor);
    else MetadataService.addWsInformations({ pathName }, target);
  };
}
