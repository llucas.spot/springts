import { ErrorRequestHandler, RequestHandler } from 'express';

export enum HttpMethod {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE',
  OPTIONS = 'OPTIONS',
}

export enum ExpressHttpMethod {
  GET = 'get',
  POST = 'post',
  PATCH = 'patch',
  PUT = 'put',
  DELETE = 'delete',
  OPTIONS = 'options',
}

export type RouteDefinition = {
  name: string,
  pathName: string,
  method: {
    expressHttpMethod: ExpressHttpMethod;
    requestHandlerName: string;
    requestHandler: ExpressFunction | ExpressErrorFunction;
  }
}

export enum MetadataKeys {
  WS_INSTANCES = 'wsInstances',
  WS_INFORMATIONS = 'wsInformations',
  WS_ROUTES_MIDDLEWARES = 'wsRoutesMiddlewares',
  WS_MIDDLEWARES = 'wsMiddlewares',
  WS_ROUTES = 'wsRoutes',
}

export type Target = any

export type WsInformations = {
  pathName: string,
}

export type WsRoutes = { [key: string]: RouteDefinition }

export type WsRoutesMiddlewares = { [key: string]: ExpressMiddlewares }

export type WsMiddlewares = ExpressMiddlewares | ExpressErrorMiddlewares

export type Decorator = (target: any, propertyKey?: string, descriptor?: PropertyDescriptor) => void

export type ExpressMiddleware = ExpressFunction | ExpressErrorMiddleware

export type ExpressMiddlewares = (ExpressMiddleware | ExpressErrorMiddleware)[]

export type ExpressNormalMiddleware = ExpressFunction

export type ExpressNormalMiddlewares = ExpressFunction[]

export type ExpressErrorMiddleware = ExpressErrorFunction

export type ExpressErrorMiddlewares = ExpressErrorMiddleware[]

export type ExpressFunction = RequestHandler

export type ExpressErrorFunction = ErrorRequestHandler

