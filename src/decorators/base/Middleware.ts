import { MetadataService } from '../../services/MetadataService/MetadataService';
import { Decorator, ExpressMiddleware, ExpressMiddlewares } from './beans';

export function Middleware(middleware: ExpressMiddleware | ExpressMiddlewares): Decorator {
  return (target, propertyKey, descriptor) => {
    if (propertyKey) MetadataService.addWsRouteMiddleware(middleware, propertyKey, target.constructor);
    else MetadataService.addWsMiddleware(middleware, target);
  };
}
