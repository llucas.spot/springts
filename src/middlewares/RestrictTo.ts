import { NextFunction, Request, Response } from 'express';
import HttpError from 'http-errors';
import { AuthenticateOptions, PassportStatic } from 'passport';
import { Ws } from '../beans/Ws';
import { Decorator } from '../decorators/base/beans';
import { Middleware } from '../decorators/base/Middleware';
import { SpringAuthenticatedRequest } from '../types/SpringAuthenticatedRequest';
import { SpringNextFunction } from '../types/SpringNextFunction';
import { SpringRequest } from '../types/SpringRequest';
import { SpringResponse } from '../types/SpringResponse';

enum StrategyPrefix {
  JWT = 'JWT_',
}

const getOptions = (strategyName: string) => {
  const options: AuthenticateOptions = {};
  if (strategyName.includes(StrategyPrefix.JWT)) options.session = false;
  options.failWithError = true;
  return options;
};

export function RestrictToPermissionsMaker(passport: PassportStatic) {
  function RestrictTo(strategyName: string, permission: string | string[]): Decorator {
    const permissions = Array.isArray(permission) ? permission : [permission];
    const options = getOptions(strategyName);
    return Middleware([
      function (this: Ws, req: SpringRequest, res: SpringResponse, next: SpringNextFunction) {
        return passport.authenticate(strategyName, options)(req, res, next);
      },
      function (this: Ws, err, req: Request, res: Response, next: NextFunction) {
        next(err);
      },
      // @ts-ignore exception
      function (this: Ws, req: SpringAuthenticatedRequest<any>, res: SpringResponse, next: SpringNextFunction) {
        for (permission of permissions) {
          if (!req.authInfo.permissions.includes(permission)) {
            this.logger.warn(`Permission ${permission} not found in user's permissions ${req.authInfo.permissions}`);
            return next(new HttpError.Forbidden());
          }
        }
        return next();
      },
    ]);
  }

  return RestrictTo;
}
