import { NextFunction, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import HttpError from 'http-errors';
import Joi from 'joi';
import { Base } from '../beans/Base';
import { Middleware } from '../decorators/base/Middleware';
import { SpringRequest } from '../types';
import { DEFAULT_JOI_OPTION } from './ValidateBodyWithJoi';

export function ValidatePathParamsWithJoi<T>(
  schema: Joi.ObjectPropertiesSchema<T>,
) {
  return Middleware(function (this: Base, req: SpringRequest, res: Response, next: NextFunction) {
    const {
      error,
      value,
    } = Joi.object<T>(schema).validate(req.params, DEFAULT_JOI_OPTION);
    if (error) {
      this.logger.warn('BadRequest : params error: ', error);
      return next(new HttpError.BadRequest());
    }
    req.params = value as ParamsDictionary;
    return next();
  });
}
