import * as bodyParser from 'body-parser';
import { NextFunction, Request, Response } from 'express';
import HttpError from 'http-errors';
import Joi, { ValidationOptions } from 'joi';
import { Base } from '../beans/Base';
import { Middleware } from '../decorators/base/Middleware';

type PickRecursive<T extends object, U extends keyof T> = U extends object ? PickRecursive<T, U> : Pick<T, U>

export const DEFAULT_JOI_OPTION: ValidationOptions = {
  abortEarly: false,
  stripUnknown: {
    arrays: true,
    objects: true,
  },
};

export function JoiBodyValidatorMiddlewareBuilder<T>(bodyToJsonMiddleware: typeof bodyParser.json) {
  return function JoiBodyValidatorMiddleware(
    schema: Joi.ObjectPropertiesSchema<T>,
  ) {
    return Middleware([
      bodyToJsonMiddleware(),
      function (this: Base, req: Request, res: Response, next: NextFunction) {
        console.log('body : ', req.body);
        const {
          error,
          value,
        } = Joi.object<T>(schema).validate(req.body, DEFAULT_JOI_OPTION);
        if (error) {
          this.logger.warn('BadRequest : body error: ', error);
          return next(new HttpError.BadRequest());
        }
        req.body = value;
        console.log('body cleaned : ', req.body);
        return next();
      }]);
  };
}
