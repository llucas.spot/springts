import { container, DependencyContainer, inject, Lifecycle, singleton } from 'tsyringe';
import { constructor } from 'tsyringe/dist/typings/types';

type Injector = DependencyContainer;

let globalInjector: Injector = container.createChildContainer();

export function getGlobalInstance<T>(service: string | constructor<T>): T {
  if (!globalInjector) {
    throw new Error(
      'Global injector is not defined, ' +
      'you must call "configureGlobalInjector" before trying to use "getInstance"',
    );
  }
  return globalInjector.resolve<T>(service);
}

export function injectSingleton<T>(token: string, provider: constructor<T>) {
  if (!globalInjector) {
    throw new Error(
      'Global injector is not defined, ' +
      'you must call "configureGlobalInjector" before trying to use "getInstance"',
    );
  }
  globalInjector.register<T>(token, provider, {
    lifecycle: Lifecycle.Singleton,
  });
}

export { inject, singleton };
