#!/bin/bash

# @types/node
sed -i '1s#^#// @ts-nocheck \n#' ./node_modules/@types/node/globals.d.ts
